import NavBar from "./components/menu";
import Header from "./components/header";
import Offers from "./components/offers";
import Contact from "./components/contact";
import Footer from "./components/footer";
import { ParallaxProvider } from 'react-scroll-parallax';

function App() {

  return (
    
    <div className="App">
      <ParallaxProvider>
        <Header/>
        <NavBar />
        <Offers />
        <Contact />
        <Footer/>
      </ParallaxProvider>
    </div>
  );
}

export default App;
