import React from "react";

function Footer() {
    const newDate = new Date()
    const year = newDate.getFullYear()

    return (
        <div className="footer__container">
            <p>ncDevWeb@{year}</p>
            <p>--micro-entrepreneur --SIREN 915 101 083 00011</p>
        </div>
    )
}

export default Footer