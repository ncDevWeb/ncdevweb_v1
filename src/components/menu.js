import React from 'react'
import { useEffect, useState } from "react";

import Logo from "../images/logo_300.png";

function NavBar() {
    const [showNavbar, setShowNavbar] = useState(false)
    
    useEffect(() => {
        window.addEventListener('scroll', () =>{
            const {scrollTop} = document.documentElement
            if(scrollTop > 100) {
                setShowNavbar(true)
            } else setShowNavbar(false)
        })
    })

    return (
        <div className={showNavbar ? "navbar navbar-active" : "navbar"}>
            <div className='navbar__logo'>
                <img src={Logo} alt="logo ncDevWeb" />
            </div>
            <div className='navbar__links'>
                <button >espace client</button>
                <button >contact</button>
            </div>
        </div>
    )
}

export default NavBar
