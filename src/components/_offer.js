import React, { useState } from "react";

function Offer(props){
    const [isHover, setIsHover] = useState(false);
    const id = "button" + props.id

    return (
        <button
            disabled={props.disabled}
            className="offer__container"
            onMouseEnter={() => setIsHover(true) }
            onMouseLeave={() => setIsHover(false) }
            id={id}    
            >
            {isHover
                ?
                <div className="offer__hover">
                    <h4>{props.hover}</h4>
                </div>
                :
                <div className="offer">
                    <h3>{props.title}</h3>
                    <img src={props.image} alt="Illustration" />
                    <p>{props.description}</p>
                </div>
            }
        </button>
    )
}

export default Offer;