import React from "react";

import Formulaire from "./_contact__formulaire.js"
import DatasApp from "../db/app.json"

function Contact() {

    const Datas = DatasApp[1]
    return (
        <div className="contact__container">
            <div className="contact__title">
                <h2>{Datas.title}</h2>
                <img src={Datas.illustration} alt="illustration" />
            </div>
            <Formulaire/>
        </div>
    )
}

export default Contact