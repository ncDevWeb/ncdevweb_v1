import React from "react";
import { Parallax } from 'react-scroll-parallax';

function Letter3 (props) {
    const x= props.x -900
    const y= props.y -300
    return (
        <Parallax translateY={[x, y]}>
            <h4>{props.letter}</h4>
        </Parallax>
    )
}

export default Letter3