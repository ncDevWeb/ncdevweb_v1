import React from "react";
import { Parallax } from 'react-scroll-parallax';

function Letter2 (props) {
    const x= props.x
    const y= props.y -100
    return (
        <Parallax translateY={[x, y]}>
            <h4>{props.letter}</h4>
        </Parallax>
    )
}

export default Letter2