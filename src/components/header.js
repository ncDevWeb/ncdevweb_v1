import React from 'react'
import { Parallax } from 'react-scroll-parallax';

//import images
import Logo from "../images/logo_transparent.svg";

import DatasApp from "../db/app.json";

import Effect from './_header__effect';


function Header(){

    const Datas = DatasApp[0]

    return (
        <header className="app-header">
            <div className="app-header__logo">
                <Parallax translateY={[55, -40]} >
                    <img src={Logo} alt="Logo NCDevWeb" className="app-header__logo__img" />
                </Parallax>
            </div>
            <div className='app-header__sublogo'>
                <div className='letters'>
                    <Parallax translateY={[180, -220]}>
                        <h3>d</h3>
                    </Parallax>
                    <Parallax translateY={[230, -260]}>
                        <h3>e</h3>
                    </Parallax>
                </div>
                <div className='letters'>
                    <Parallax translateY={[230, -280]}>
                        <h3>e</h3>
                    </Parallax>
                    <Parallax translateY={[130, -180]}>
                        <h3>b</h3>
                    </Parallax>
                </div>
            <Effect />
            </div>
            <div className="app-header__title">
                <img src={Datas.illustration} alt="Illustration" />
                <h1>{Datas.title}</h1>
                <button className='site-button'>{Datas.button}</button>
            </div>
        </header>
    )
}

export default Header
