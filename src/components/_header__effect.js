import React, { useEffect, useState } from "react";


import Letter from "./_header__effect__letter";
import Letter2 from "./_header__effect__letter2";
import Letter3 from "./_header__effect__letter3";

function Effect() {

    function makeid(length) {
        var result           = '';
        var characters       = 'eijvwxy01/><;{}[]=+()@%$^&*!?';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
       }
       return result;
    }

    function getRandomIntInclusive(min, max) {
       min = Math.ceil(min);
       max = Math.floor(max);
        return Math.floor(Math.random() * (max - min +1)) + min;
    }
    const [lettersList, setLettersList] = useState([])
    const [lettersList2, setLettersList2] = useState([])
    const [lettersList3, setLettersList3] = useState([])
    const [lettersList4, setLettersList4] = useState([])

      useEffect(() => {
        for (var j = 0; j < 4; j++){
            setLettersList((previousList) => [...previousList, {letter: makeid(1), x: getRandomIntInclusive(90,180), y: getRandomIntInclusive(-100,-140)}]) 
        }
        for (var k = 0; k < 3; k++){
            setLettersList2((previousList) => [...previousList, {letter: makeid(1), x: getRandomIntInclusive(90,180), y: getRandomIntInclusive(-100,-140)}]) 
        }
        for (var l = 0; l < 5; l++){
            setLettersList3((previousList) => [...previousList, {letter: makeid(1), x: getRandomIntInclusive(100,160), y: getRandomIntInclusive(-210,-140)}]) 
        }
        for (var l = 0; l < 15; l++){
            setLettersList4((previousList) => [...previousList, {letter: makeid(1), x: getRandomIntInclusive(150,950), y: getRandomIntInclusive(-210,240)}]) 
        }
    },[])

    console.log(lettersList)

    return (
        <div className="effect__container">
            <div className="letters__container letter-resize2">
                {lettersList.map((letter, index) => {
                    return <Letter key={index} {...letter}/>
                })}
            </div>
            <div className="letters__container letter-resize">
                {lettersList2.map((letter2, index) => {
                    return <Letter2 key={index} {...letter2}/>
                })}
            </div>
            <div className="letters__container letter-resize3">
                {lettersList3.map((letter3, index) => {
                    return <Letter3 key={index} {...letter3}/>
                })}
            </div>
            <div className="letters__container letter-resize4">
                {lettersList4.map((letter4, index) => {
                    return <Letter3 key={index} {...letter4}/>
                })}
            </div>
        </div>
    )
}

export default Effect