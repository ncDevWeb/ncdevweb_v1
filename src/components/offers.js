import React from "react";
import Offer from "./_offer";
import OffersList from "../db/offers.json"

function Offers() {
    const buttons = document.querySelectorAll(".offer__container")
    

    return (
        <div className="offers__container" id="offers">
            {OffersList.map(offer => {
                return <Offer key={offer.id} {...offer}/>
            })}
        </div>
    )
}

export default Offers